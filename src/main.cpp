#include <SDL2/SDL.h>
#include <stdio.h>
#include <assert.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#ifdef __EMSCRIPTEN__
#include <emscripten.h>
#endif

#include "opengl.h"
#include "ShaderProgram.h"

#undef main

SDL_Window* window;
SDL_Renderer* renderer;
SDL_GLContext context;
GLuint vbo;
mimi::ShaderProgram program;
bool running = true;

// opengl errors
void glDebugMessage(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam) {
  fprintf(stderr, "OpenGL Error: %s\n", message);
}

// render game
void render() {
  SDL_Event evt;
  while (SDL_PollEvent(&evt)) {
    if (evt.type == SDL_QUIT || (evt.type == SDL_KEYDOWN && evt.key.keysym.scancode == SDL_SCANCODE_ESCAPE)) {
      running = false;

#ifdef __EMSCRIPTEN__
      emscripten_cancel_main_loop();
#endif
    }
  }

  // Attributes
  const GLuint posAttrib = program.attribLoc("in_position");

  // Clear
  glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
  glClear(GL_COLOR_BUFFER_BIT);

  // Bind program
  program.bind();

  static float rot = 0.0f;
  rot += 0.001f;
  glm::mat4 m = glm::rotate(glm::mat4(1.0f), rot, glm::vec3(0.0f, 1.0f, 0.0f));

  // Set uniforms
  //program.setUniform("uni_projection", m);
  //program.setUniform("col", glm::vec4(1.0f, 0.5f, 0.0f, 0.0f));

  // Bind buffers
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glEnableVertexAttribArray(posAttrib);
  glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 0, 0);

  if (program.valid())
    glDrawArrays(GL_TRIANGLES, 0, 3);

  glDisableVertexAttribArray(posAttrib);

  SDL_GL_SwapWindow(window);
}

// entry point
int main() {
  printf("starting up\n");

  // Initialise SDL
  assert(SDL_Init(SDL_INIT_VIDEO) == 0);

  // Create window
  SDL_CreateWindowAndRenderer(800, 800, SDL_WINDOW_OPENGL, &window, &renderer);
  context = SDL_GL_CreateContext(window);

#ifndef __EMSCRIPTEN__
  // Load opengl extensions
  glewInit();

  // Enable debug output
  glEnable(GL_DEBUG_OUTPUT);
  glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, false);
  glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_HIGH, 0, nullptr, true);
  glDebugMessageCallback(glDebugMessage, nullptr);
#endif

  // Create vbo
  float buf[] = {
    0.0f, 1.0f,
    -1.0f, -1.0f,
    1.0f, -1.0f
  };

  glGenBuffers(1, &vbo);
  glBindBuffer(GL_ARRAY_BUFFER, vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(buf), buf, GL_STATIC_DRAW);

  // Load shader
  program.loadProgram("resources/color.glsl");

#ifdef __EMSCRIPTEN__
  // Set up render function
  emscripten_set_main_loop(render, 0, 1);
#else
  while (running) {
    render();
  }
#endif

  // Cleanup
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
  SDL_Quit();
}
