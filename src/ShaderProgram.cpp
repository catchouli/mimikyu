#include "ShaderProgram.h"
#include "opengl.h"
#include <stdio.h>
#include <malloc.h>

namespace mimi {

  ShaderProgram::ShaderProgram(const char* filename)
  {
    if (filename) {
      loadProgram(filename);
    }
  }

  ShaderProgram::~ShaderProgram()
  {
    unloadProgram();
  }

  void ShaderProgram::bind()
  {
    if (m_program == 0) {
      fprintf(stderr, "Tried to bind invalid shader program\n");
    }

    glUseProgram(m_program);
  }

  void ShaderProgram::unloadProgram() {
    if (m_program != 0) {
      glDeleteProgram(m_program);
      m_program = 0;
    }
  }

  // Load shader program
  void ShaderProgram::loadProgram(const char* filename) {
    // Unload program if there's already one loaded
    unloadProgram();

    // Read file into string
    FILE* file = fopen(filename, "rb");
    if (!file) {
      fprintf(stderr, "Failed to open shader %s\n", filename);
      return;
    }

    // Get length of file
    fseek(file, 0, SEEK_END);
    long len = ftell(file);
    fseek(file, 0, SEEK_SET);

    // Allocate memory
    char* buf = (char*)malloc(len+1);

    // Read full file
    size_t read = fread(buf, 1, len, file);
    buf[len] = 0;

    // Specify different version when compiling on desktop target
  #ifdef __EMSCRIPTEN__
    const char* version = "#version 100\n";
  #else
    const char* version = "#version 120\n";
  #endif

    // Load shaders
    const char* vertSrcs[] = { version, "#define BUILDING_VERTEX_SHADER\n", buf };
    const char* fragSrcs[] = { version, "#define BUILDING_FRAGMENT_SHADER\n", buf };
    GLuint vert = compileShader(GL_VERTEX_SHADER, (const char**)vertSrcs, 3);
    GLuint frag = compileShader(GL_FRAGMENT_SHADER, (const char**)fragSrcs, 3);

    // Free memory
    fclose(file);
    free(buf);

    // Check compile succeeded
    if (vert == 0 || frag == 0) {
      fprintf(stderr, "Shader compilation failed\n");
      return;
    }

    // Create program
    GLuint prog = glCreateProgram();

    glAttachShader(prog, vert);
    glAttachShader(prog, frag);
    glLinkProgram(prog);

    glDeleteShader(vert);
    glDeleteShader(frag);

    // Check link status
    GLint linked;
    glGetProgramiv(prog, GL_LINK_STATUS, &linked);
    if (!linked) {
      GLint infoLen = 0;

      glGetProgramiv(prog, GL_INFO_LOG_LENGTH, &infoLen);

      if (infoLen > 0) {
        char* infoLog = (char*)malloc(sizeof(char) * infoLen);

        glGetProgramInfoLog(prog, infoLen, nullptr, infoLog);
        fprintf(stderr, "Error linking shader:\n%s\n", infoLog);

        free(infoLog);
      }

      glDeleteProgram(prog);
      return;
    }

    // Store id
    m_program = prog;

    // Cache uniforms and attributes
    updateCache();
  }

  // Load shader
  GLuint ShaderProgram::compileShader(GLenum type, const char** sources, int count) {
    GLuint shader = glCreateShader(type);

    if (shader == 0)
      return 0;

    glShaderSource(shader, count, sources, nullptr);
    glCompileShader(shader);

    // Check compile status
    GLint compiled;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
    if (!compiled) {
      GLint infoLen = 0;

      glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);

      if (infoLen > 0) {
        char* infoLog = (char*)malloc(sizeof(char) * infoLen);

        glGetShaderInfoLog(shader, infoLen, nullptr, infoLog);
        fprintf(stderr, "Error compiling shader:\n%s\n", infoLog);

        free(infoLog);
      }

      glDeleteShader(shader);
      return 0;
    }

    return shader;
  }

  void ShaderProgram::updateCache() {
    GLint count;

    const GLsizei bufSize = 16;
    GLchar name[bufSize];
    GLenum type;
    GLint size;
    GLsizei length;

    m_attribLocs.clear();
    m_uniformLocs.clear();

    // Get active attributes
    glGetProgramiv(m_program, GL_ACTIVE_ATTRIBUTES, &count);
    for (GLint i = 0; i < count; ++i) {
      glGetActiveAttrib(m_program, (GLuint)i, bufSize, &length, &size, &type, name);
      GLint loc = glGetAttribLocation(m_program, name);
      m_attribLocs[name] = loc;
    }

    // Get active uniforms
    glGetProgramiv(m_program, GL_ACTIVE_UNIFORMS, &count);
    for (GLint i = 0; i < count; ++i) {
      glGetActiveUniform(m_program, (GLuint)i, bufSize, &length, &size, &type, name);
      GLint loc = glGetUniformLocation(m_program, name);
      m_uniformLocs[name] = loc;
    }
  }

  GLuint ShaderProgram::uniformLoc(const char* name)
  {
    auto it = m_uniformLocs.find(name);
    if (it == m_uniformLocs.end())
      return -1;
    else
      return (GLuint)(it->second);
  }

  GLuint ShaderProgram::attribLoc(const char* name)
  {
    auto it = m_attribLocs.find(name);
    if (it == m_attribLocs.end())
      return -1;
    else
      return (GLuint)(it->second);
  }

}