#pragma once

#include "opengl.h"
#include <glm/glm.hpp>
#include <vector>
#include <unordered_map>
#include <string>

namespace mimi
{
  class ShaderProgram
  {
  public:

    // Load shader program from file, or create an unloaded shader program
    ShaderProgram(const char* filename = nullptr);

    // Clean up program
    ~ShaderProgram();

    // Load a shader program from a file
    void loadProgram(const char* filename);

    // Unload the loaded shader program
    void unloadProgram();

    // Bind the shader program
    void bind();

    // Check if the shader program is valid
    bool valid() const { return m_program != 0; };
    
    // Get uniform location
    GLuint uniformLoc(const char* name);

    // Get attribute location
    GLuint attribLoc(const char* name);

    // Set uniform value
    template <typename T>
    void setUniform(const char* name, const T& value);

  private:

    // Compile a shader
    GLuint compileShader(GLenum type, const char** sources, int count);

    // Update cached uniforms etc
    void updateCache();

    // Set specific uniform value
    template <typename T>
    void setUniformValue(GLuint loc, const T& value);

    // The opengl name of the program
    unsigned int m_program = 0;

    // Map of uniform locations
    std::unordered_map<std::string, GLint> m_uniformLocs;

    // Map of attribute locations
    std::unordered_map<std::string, GLint> m_attribLocs;

  };

  template <typename T>
  inline void ShaderProgram::setUniform(const char* name, const T& value)
  {
    bind();
    GLint loc = uniformLoc(name);
    if (loc >= 0) {
      setUniformValue(loc, value);
    }
  }

  template <>
  inline void ShaderProgram::setUniformValue(GLuint loc, const glm::mat4& value)
  {
    glUniformMatrix4fv(GLint(loc), 1, false, (float*)&value);
  }

  template <>
  inline void ShaderProgram::setUniformValue(GLuint loc, const glm::vec3& value)
  {
    glUniform3fv(GLint(loc), 1, (float*)&value);
  }

  template <>
  inline void ShaderProgram::setUniformValue(GLuint loc, const glm::vec4& value)
  {
    glUniform4fv(GLint(loc), 1, (float*)&value);
  }

  template <typename T>
  inline void ShaderProgram::setUniformValue(GLuint loc, const T& value)
  {
    static_assert(std::is_same<T,T>::value, "Not implemented");
  }
}
