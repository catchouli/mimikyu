precision mediump float;

#ifdef BUILDING_VERTEX_SHADER

attribute vec3 in_position;

uniform mat4 uni_projection;

void main() {
  gl_Position = uni_projection * vec4(in_position, 1.0);
}

#endif


#ifdef BUILDING_FRAGMENT_SHADER

uniform vec4 col;

void main() {
  gl_FragColor = vec4(col);
}

#endif
