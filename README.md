Emscripten build:
 - Make sure you've run emsdk_env.sh
 - Run ./generate to generate a cmake build in the build folder
 - cd build && make
